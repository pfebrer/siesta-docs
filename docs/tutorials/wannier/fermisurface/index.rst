:sequential_nav: next

..  _tutorial-wannier-fermisurface:

Plotting of Fermi surface with Wannier functions (example: :math:`SrTiO_3`)
===========================================================================

In this tutorial, we'll learn how to plot the Fermi surface with Wannier functions. We'll continue with the example of :math:`SrTiO_3` in the cubic phase. You can follow the slides which can be downloaded with the link below:

* :download:`Exercise-Fermi-surface-SrTiO3.pdf<work-files/Exercise-Fermi-surface-SrTiO3.pdf>`

You can download the needed file for this tutorial.
   

* :download:`Sr.psf<work-files/Sr.psf>`
* :download:`Ti.psf<work-files/Ti.psf>`
* :download:`O.psf<work-files/O.psf>`
* :download:`SrTiO3.fdf<work-files/SrTiO3.fdf>`
* :download:`SrTiO3.win<work-files/SrTiO3.win>`


.. _introduction:


Tutorials
===========

This set of tutorials will guide you in the exploration of Siesta's features.

**Before you do anything else, start here.** You need to set up your local
working environment to follow the tutorial.

..  toctree::
    :maxdepth: 1

    01-installation.rst


Basics of Siesta
----------------

This section is recommended for all beginners, and also as a refresher
for more experienced users.

..  toctree::
    :maxdepth: 1
    
    basic/first-encounter/index
    basic/first-crystals/index 
    basic/pseudopotentials/index
    basic/basis-sets/index
    basic/analysis-tools/index
    basic/grid-convergence/index
    basic/kpoint-convergence/index
    basic/scf-convergence/index
    basic/structure-optimization/index
    basic/vibrational-properties/index
    basic/magnetism/index
    basic/basic-input-file.rst
    basic/basic-output-files.rst
  

Intermediate and Advanced Topics
--------------------------------

This section provides deeper introductions to relevant topics.

..  toctree::
    :maxdepth: 1

    molecular-dynamics/index.rst
    td-dft/index.rst
    spin-orbit/index.rst
    polarization/index.rst
    dft+u/index.rst
    wannier/index.rst
    pseudopotentials/index.rst
    advanced-analysis/index.rst
    phonons/index
    optical-properties/index.rst
    tb2j/index.rst
    stm-images/index
    neb/index.rst
    unfolding/index
    building-deployment/siesta-dep-docs.rst
    
    


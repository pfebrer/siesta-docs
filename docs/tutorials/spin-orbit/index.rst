:sequential_nav: next

..  _tutorial-spin-orbit:

Spin-Orbit coupling
===================

:author: Ramón Cuadrado


*Purpose:* The main goal of the next four basic examples is to give to
the user an initial guess to perform calculations using the Spin-Orbit
implementation of SIESTA.

.. note::
   It is mandatory to take into account when the SO is included in
   the calculation the following tips: converge the k–points sampling and
   mesh cutoff, density matrix tolerance below 10\ :sup:`−\ 5`\ eV, use
   relativistic pseudopotentials (PPs) and use the *non–linear core
   corrections* when the PPs are built. All the information regarding the
   basis set and all the parameters used in a common SIESTA calculation are
   valid for the SO. In the next two examples the above mentioned
   parameters (mesh cutoff, etc) are not converged because we want to speed
   up the calculations to show an initial (and dirty) results, but
   remember, for a real calculation those values have to be converged. *An
   additional advice*: *remember to read the section related with the SOC
   in the main SIESTA manual!*

Example 1. – Calculation of the magnetic anisotropy of Pt\ :sub:`2`.

Given the *example_1z.fdf* file and the pseudopotential of Pt atom,
*Pt_pt2_SOC.psf*, calculate the total selfconsistent energy E\ *tot* for
three highly symmetric orientations of the initial magnetization,
namely, X, Y and Z, keeping fixed the physical dimer (the (x,y,z)
coordinates of each atom). Each calculation has to be initialized
modifying the *#.fdf* block: DM.InitSpin, updating the orientation of
each atomic magnetization. As a result one has to have three different
energy values for each (*θ*,\ *ϕ*) pair. Remember to rename each one of
the *#.fdf* files (and label flag) for each magnetic orientation and do
not use the density matrix obtained in other orientations as it has to
be calculated for each specific magnetic configuration from scratch.

*Advanced:* One can try to change the orientation of the dimer (say
put it along Z or Y axes) and compute again the three energy values and
compare the results for both orientations. Some of the total energies
have to be the same.

Example 2. – Calculation of the magnetic anisotropy of FePt bulk.

Given *example_2z.fdf* file and the PPs of Fe and Pt, *Fe_fept_SOC.psf*
and *Pt_fept_SOC.psf*. Each calculation has to be initialized modifying
the *#.fdf* block: DM.InitSpin, updating the orientation of each atomic
magnetization. As a result one has to have three different energy values
for each (*θ*,\ *ϕ*) pair.

A) Obtain the E\ *tot* for three highly symmetric orientations of the
initial magnetization, namely, X, Y and Z, keeping fixed the atoms in
their unit cell. Again do not use the same density matrix for same
calculations. Allow SIESTA to calculate it from scratch.

B) Perform a magnetization curve changing the angles at intervals of 20
degrees from the Z 1

inital orientation to finalize along X axis and plot the resulting
energy as a function of the varied angle. One can check the MMs, charge,
etc. These set of calculations will take lot of time to be performed
completely, so you can try a few steps and see how the total energy
evolves.

Example 3. – Spin Projected Density of States (collinear case).

Within the *example_3.fdf* is included the block Projected density of
states. After SIESTA runs we will have a file called *example_3.DOS* and
*example_3.PDOS*. There will be the information of the DOS and PDOS,
respectively.

Check *example_3.fdf* file, modified if needed and run SIESTA after the
modification.

Plot the example_3.DOS using gnuplot. In doing this you will see the
total DOS of the system. If you want to see the atomic resolved DOS
(PDOS on atoms, for example), you have to extract from the PDOS file the
parts that represent the Fe/Pt DOS.

Advanced: By means of the post–processing utility of A. Postnikov,
located in /tmp/SIESTA-OKP/Util/Contrib/APostnikov (or maybe you want to
program your own code in fortran, python, etc, to extract the data to a
more readable format) you can extract the data of *#.PDOS* file and
re–structure it in columns to be able to plot using gnuplot (or any
other program). You can plot the PDOS\ *↑*, PDOS\ *↓*\ for each specie
or the total DOS (the sum of all the columns). It is worth to noting
that within the PDOS file is “proyected” the DOS on specific atomic
orbitals and you can extract the information that you want. Note: In
general, for the transition metals the most relevant orbitals will be
the *d* ones.

Example 4. – Projected Density of States (fully relativistic case).

When the Spin–Orbit coupling is included in the calculation the PDOS and
DOS files will have four columns corresponding to PDOS\ *↑*, PDOS\ *↓*,
and Re PDOS\ *↑↓* and Im PDOS\ *↑↓* such that the integral of Re
PDOS\ *↑↓* up to E\ *F* will give the magnetization along *X* axis,
M\ *x*, and the integral of Im PDOS\ *↑↓* up to E\ *F* , M\ *y*

*Note:* For a noncollinear calculation without SOC, the files will have
the same structure as those for SOC and hence four columns will be
written in the DOS/PDOS output files.

Run SIESTA for the *example_4z.fdf* file in which is included the SOC
flag and plot the total DOS. You can use the selfconsistent density
matrix (*label.DM*) to calculate the DOS/PDOS. In this case the
calculation is quite quick and the calculation of the DM from scratch is
fast but in other cases it would be better to have the DM file and not
calculated again. So if you want copy it from the calculation of
*Example 2* and use it!

Advanced: You can play with Postnikov tool to plot some selective PDOS
curves. 2

:sequential_nav: next

..  _tutorial-basic-first-crystals:

First crystals
==============

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

In this exercise we will move beyond molecules to treat crystals, that
is, periodic solids, also known as `bulk systems`. 

.. note::
   In this tutorial, the main objective is to present input topics
   (mandatory unit cell, k-point sampling), and outputs specific to
   crystals (e.g, band structures and DOS), and also the distinction
   between insulators and metals. There is currently some
   discussion of "k-point convergence", which might overlap with a
   fuller treatment in another module.

In bulk systems the electronic states can be characterized by a
continuous quantum number k, the so-called Bloch vector or crystalline
quasi-momentum of the electrons.  The space of allowed Bloch vector
(the Brillouin zone) is usually sampled using a finite grid. The most
popular scheme to generate this integration mesh is the Monkhorst-Pack
algorithm. This is also used in SIESTA.  The integration grid in the
Brillouin zone is specified using the block kgrid_Monkhorst_Pack::

  (monkhorst-pack block for MgO)
  

We will generate the band-structure for MgO and look at the density of
states...

.. warning::
   Rest of file under construction using
   material from the old `ElectronicStructure` directory.
   
To plot the DOS you will use the utility program 'Eig2DOS'. This
program reads the information contain in the file MgO.EIG which
contains all the eigenvalues for each k-point used to sample the BZ, and the MgO.KP file
which contains the k-point sampling information.  Useful options to
the program (type 'Eig2DOS -h' for a full list) are the broadening for
each state in eV (a value of the order of the default (0.2 eV) is
usually reasonable), the number of energy points where the DOS will be
calculated (200 by default) and the Emin and Emax of the energy window
where the DOS will be calculated (the default is to compute the DOS in
the whole range of energies available in the EIG file).

For example::

   Eig2DOS -k MgO.KP  MgO.EIG > dos 

will compute the DOS in the whole range, using a grid of 200 points, a
broadening ("smearing") of 0.2 eV, and using the k-point info from
MgO.KP. (The k-point file information is important when different
k-points have different weights).

The file MgO.fdf will also produce a file MgO.bands containing the
band structure along the several high-symmetry lines in the Brillouin zone (BZ) as
specified using the block BandLines::

  BandLineScale
  %block BandLines
  ...

To learn more about the special high-symmetry points and lines of the
BZ for this case, you can visit the `Bilbao Crystallographic Server
<https://www.cryst.ehu.es/cgi-bin/cryst/programs/nph-kv-list?gnum=225&fig=fm3qmf>`_
or `this site
<http://lamp.tu-graz.ac.at/~hadley/ss1/bzones/fcc.php>`_.

(Note that the final Gamma point in the BandLines sequence is actually
an image along (1,1,1) of the Gamma point at the origin.)

To plot the band structure you need to use the utility program 'gnubands'::

 gnubands < MgO.bands > out.bands

and you can plot 'out.bands' with 'gnuplot', using the 'bands.gplot'
file provided (which has extra information to place and label the
'ticks' for each symmetry point in the BZ)::

  gp bands.gplot


.. note::
   Enter the directory 'Al'

For metals such as Al there are electronic bands that are not completely filled
and therefore for an accurate description of the total energy, forces
and all properties of the materials it is necessary to use a better
sampling in reciprocal space (Bloch vectors) than for insulators. In
the input file Al_bulk.fdf a 4x4x4 grid is used. This might be
insufficient for a good description of aluminium.

.. note::
   Use eig2bxsf to get the Fermi surface?
   
You should explore the convergence of total energy, the lattice
parameter, and density of states respect to the fineness of the
k-sampling.

Also, it is instructive to see the behavior of the DOS when the
k-point sampling gets more dense. For coarse samplings, it does not
look at all like the "free-electron-like" curve we see in
textbooks. This is due to the very simple method used to construct the
DOS (just broadening a collection of discrete energy levels).

A SZ basis set is specified in the file Al_bulk.fdf.  It might be
quite interesting to see how the band structure changes when more
complete basis sets are used (DZ,DZP). You might defer this for the
:ref:`tutorial on Basis Sets <tutorial-basic-basis-sets>`.
